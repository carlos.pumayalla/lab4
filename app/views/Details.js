import React from 'react'
import { Text, StyleSheet, Dimensions, View, Image } from 'react-native'
  
const Details = ({ navigation }) => {
    const _id = navigation.getParam('_id')
    const tilte = navigation.getParam('title')
    const dcr = navigation.getParam('descrip')
    const img = navigation.getParam('img')
    return (
      <>
        <View style={styles.container}>
            <Text style={{ fontSize: 48 }}>{tilte}</Text>
            <Image style={styles.image} source={img}></Image>
            <Text style={{ fontSize: 18 }}>{dcr}</Text>
        </View>
      </>
    )
  }

  const styles = StyleSheet.create({
    container:{
        paddingTop: 30,
        alignItems: 'center'
    },
    text2: {
      alignItems: 'center',
      backgroundColor: '#444',
      color: 'white',
      fontWeight: 'bold',
      padding: 10,
    },
    image: {
        width: '50%',
        height: 200,
      
    },
      list: {
        width: '100%',
      }
  });

export default Details